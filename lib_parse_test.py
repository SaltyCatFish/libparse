import unittest
from lib_parse import WebParse


class Test_Lib_Parse(unittest.TestCase):

    def setUp(self):
        self.lib_parse = WebParse()
        self.test_string = "Hello, World!"
        self.delimiter = ","
        self.tag = '<meta property="og:image" itemprop="image primaryImageOfPage" content="http://cdn.sstatic.net/stackoverflow/img/apple-touch-icon@2.png?v=73d79a89bded&a" />'

    def tearDown(self):
        pass

    def test_split_string_before_include(self):
        expect = "Hello,"
        test = self.lib_parse.split_string(
            self.test_string, self.delimiter, self.lib_parse.Pivot.BEFORE, self.lib_parse.Aggregate.INCLUDE)
        self.assertEqual(test, expect)

    def test_split_string_after_include(self):
        expect = ", World!"
        test = self.lib_parse.split_string(
            self.test_string, self.delimiter, self.lib_parse.Pivot.AFTER, self.lib_parse.Aggregate.INCLUDE)
        self.assertEqual(test, expect)

    def test_split_string_before_exclude(self):
        expect = "Hello"
        test = self.lib_parse.split_string(
            self.test_string, self.delimiter, self.lib_parse.Pivot.BEFORE, self.lib_parse.Aggregate.EXCLUDE)
        self.assertEqual(test, expect)

    def test_split_string_after_exclude(self):
        expect = " World!"
        test = self.lib_parse.split_string(
            self.test_string, self.delimiter, self.lib_parse.Pivot.AFTER, self.lib_parse.Aggregate.EXCLUDE)
        self.assertEqual(test, expect)

    def test_split_string_no_delimiter_found(self):
        expect = self.test_string
        test = self.lib_parse.split_string(
            self.test_string, "x", self.lib_parse.Pivot.AFTER, self.lib_parse.Aggregate.EXCLUDE)
        self.assertEqual(test, expect)

    def test_get_string_between_include(self):
        expect = "ello, Wor"
        test = self.lib_parse.get_string_between(
            self.test_string, "e", "r", self.lib_parse.Aggregate.INCLUDE)
        self.assertEqual(test, expect)

    def test_get_string_between_exclude(self):
        expect = "llo, Wo"
        test = self.lib_parse.get_string_between(
            self.test_string, "e", "r", self.lib_parse.Aggregate.EXCLUDE)
        self.assertEqual(test, expect)

    def test_get_string_between_include_edge_case(self):
        expect = self.test_string
        test = self.lib_parse.get_string_between(
            self.test_string, "H", "!", self.lib_parse.Aggregate.INCLUDE)
        self.assertEqual(test, expect)

    def test_get_string_between_exclude_edge_case(self):
        expect = "ello, World"
        test = self.lib_parse.get_string_between(
            self.test_string, "H", "!", self.lib_parse.Aggregate.EXCLUDE)
        self.assertEqual(test, expect)

    def test_get_array_found_strings(self):
        expect = 2
        test = len(self.lib_parse.get_array_found_strings("<1><2>", "<", ">"))
        self.assertEqual(test, expect)

    def test_get_attribute_normal(self):
        expect = "og:image"
        test = self.lib_parse.get_attribute(self.tag, "property")
        self.assertEqual(expect, test)

    def test_get_attribute_spaces(self):
        expect = "image primaryImageOfPage"
        test = self.lib_parse.get_attribute(self.tag, "itemprop")
        self.assertEqual(expect, test)

    def test_get_attribute_last_attribute(self):
        expect = "http://cdn.sstatic.net/stackoverflow/img/apple-touch-icon@2.png?v=73d79a89bded&a"
        test = self.lib_parse.get_attribute(self.tag, "content")
        self.assertEqual(expect, test)

    def test_get_attributes_dict_from_tag(self):
        expect = 3
        test = len(self.lib_parse.get_attributes_dict_from_tag(self.tag))
        self.assertEqual(expect, test)

    def test_remove_text_from_string_simple(self):
        expect = "az"
        test = self.lib_parse.remove_text_from_string("abcdefghijklmnopqrstuvwxyz", "b", "y")
        self.assertEqual(expect, test)

    def test_remove_text_from_string_complex(self):
        expect = "az"
        test = self.lib_parse.remove_text_from_string("afogzfog", "f", "g")
        self.assertEqual(expect, test)

    def test_extract_domain(self):
        expect = "yahoo.com"
        test = self.lib_parse.extract_domain("https://wwww.yahoo.com")
        self.assertEqual(expect, test)




if __name__ == '__main__':
    unittest.main()
