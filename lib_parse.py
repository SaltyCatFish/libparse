
import re

from enum import Enum


class WebParse():

    def __init__(self):
        self.Pivot = Enum('Lib_parse', 'BEFORE AFTER')
        self.Aggregate = Enum('Lib_parse', 'INCLUDE EXCLUDE')

    def split_string(self, string, delimiter, pivot, aggregate):
        """ Splits a string on the delimiter, returning either the front or back of the string """

        if string.find(delimiter) == -1:
            return string

        if pivot == self.Pivot.BEFORE:
            return_value = string.split(delimiter, 1)[0]
        else:
            return_value = string.split(delimiter, 1)[1]

        if aggregate == self.Aggregate.INCLUDE:
            if pivot == self.Pivot.BEFORE:
                return_value = return_value + delimiter
            else:
                return_value = delimiter + return_value

        return return_value

    def get_string_between(self, string, starting_delimiter, ending_delimiter, aggregate):
        """ Returns a string between the starting and ending delimiter """
        start_index = string.find(starting_delimiter)
        end_index = string.find(
            ending_delimiter, start_index + len(starting_delimiter))

        if start_index == -1 or end_index == -1:
            return None

        if aggregate == self.Aggregate.EXCLUDE:
            return string[start_index + len(starting_delimiter):end_index]
        else:
            if end_index + len(ending_delimiter) > len(string):
                return string[start_index:]
            else:
                return string[start_index: end_index + len(ending_delimiter)]

    def get_array_found_strings(self, string, starting_delimiter, ending_delimiter):
        """ Returns an array of all elements between matching starting and ending delimiters """
        return_array = []

        while True:
            start_index = string.find(starting_delimiter)
            end_index = string.find(
                ending_delimiter, start_index + len(starting_delimiter))

            if start_index == -1 or end_index == -1:
                break
            else:
                if end_index + len(ending_delimiter) > len(string):
                    return_array.append(string[start_index: end_index])
                else:
                    return_array.append(
                        string[start_index: end_index + len(ending_delimiter)])
                string = string[end_index:]

        return return_array

    def get_attribute(self, tag, attribute):
        """ Returns the attribute value found in tag """
        dictionary = self.get_attributes_dict_from_tag(tag)

        if attribute in dictionary.keys():
            return dictionary[attribute]
        else:
            return None

    def get_attributes_dict_from_tag(self, tag):
        """ Returns a dictionary from a tag containing key value of attributes """
        # Determine if values are single or double quoted
        delimiter = "\""
        if tag.find("\"") == -1:
            print("Using alternate delimiter")
            delimiter = "\'"

        split_array = tag.split(delimiter)

        # Split up list into a dict, removing garbage from keys
        keys = split_array[0::2]
        regex_pattern = re.compile("^<.* ", re.IGNORECASE)
        for i in range(len(keys)):
            keys[i] = re.sub(regex_pattern, "", keys[i]
                             ).replace("=", "").strip()

        values = split_array[1::2]

        return dict(zip(keys, values))

    def remove_text_from_string(self, string, starting_delimiter, ending_delimiter):
        """ Removes all text from a string between (inclusive) the staring and
        ending delimiter """
        regex_pattern = re.compile(starting_delimiter + ".*?" + ending_delimiter, re.IGNORECASE)
        return re.sub(regex_pattern, "", string)

    def extract_domain(self, url):
        """ Extracts domain from a url """
        pattern = re.compile('([^:]*:\/\/)?([^\/]*\.)*([^\/\.]+\.[^\/]+)')
        result = re.search(pattern, url)
        return result.groups()[2]


if __name__ == '__main__':
    lib = WebParse()
    tag = "<a href='http://www.google.com' class='test_class'/>"
    print(lib.get_attribute(tag, "href"))
